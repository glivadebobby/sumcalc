package in.mdi.sumcalculator;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText mEditTextInputA, mEditTextInputB;
    private TextView mTextViewResult;
    private Button mButtonSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mEditTextInputA = (EditText) findViewById(R.id.input_a);
        mEditTextInputB = (EditText) findViewById(R.id.input_b);
        mTextViewResult = (TextView) findViewById(R.id.txt_result);
        mButtonSubmit = (Button) findViewById(R.id.btn_submit);

        mButtonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputA = mEditTextInputA.getText().toString().trim();
                String inputB = mEditTextInputB.getText().toString().trim();
                if (inputA.isEmpty()) {
                    mEditTextInputA.setError("Cannot be empty");
                } else if (inputB.isEmpty()) {
                    mEditTextInputB.setError("Cannot be empty");
                } else {
                    int a = Integer.parseInt(inputA);
                    int b = Integer.parseInt(inputB);
                    calculateSum(a, b);
                }
            }
        });
    }

    private void calculateSum(int a, int b) {
        int c = a + b;
        mTextViewResult.setText("Sum is " + c);
    }
}
